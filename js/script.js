export const fetchApi = (url) => {
    return fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw 'Error http code: ' + response.status;
            }
        })
}

export const fetchApiPost = (url, data) => {
    return fetch(url, {
        method: 'POST',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw 'Error http code: ' + response.status;
        }
    }) 
}

export const getParamFromUrl = (param) => {
    const query = window.location.search;
    const urlParam = new URLSearchParams(query);

    return urlParam.get(param);
}

export const formatPrice = (price) => {
    return new Intl.NumberFormat("fr-FR", {style: "currency", currency: "EUR"}).format(price / 100);
}
