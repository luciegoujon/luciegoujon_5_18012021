import {fetchApi, getParamFromUrl, formatPrice} from './script.js';

const addToCart = document.createElement("button");

const displayProduct = (teddy) => {
    const mainProduct = document.getElementById("main--product");
        const img = document.createElement("img");
        img.setAttribute("id", "image");
        img.setAttribute("alt", `Ours en peluche ${teddy.name}`);
        img.src = teddy.imageUrl;

        const name = document.createElement("h1");
        name.setAttribute("id", "title");
        name.textContent = `Ours en peluche ${teddy.name}`;

        const description = document.createElement("p");
        description.textContent = teddy.description;
        description.setAttribute("id", "description");

        const price = document.createElement("p");
        price.textContent = formatPrice(teddy.price);
        price.setAttribute("id", "price");

        const options = document.createElement("div");
        options.setAttribute("id", "options");

        const label = document.createElement("label");
        label.textContent = "Options";
        label.setAttribute("for", "option");

        const select = document.createElement("select");
        select.setAttribute("id", "option");
        select.setAttribute("name", "option");

        const chooseOption = document.createElement("option");
        chooseOption.setAttribute("value", "");
        chooseOption.textContent = "--Veuillez choisir une couleur--";
        select.appendChild(chooseOption);

        const colors = teddy.colors;
        let value = 0;
        colors.forEach(color => {
            let colorTeddy = document.createElement("option");
            colorTeddy.setAttribute("value", `${value}`);
            colorTeddy.textContent = color;
            select.appendChild(colorTeddy);
            value ++;
        });

        addToCart.setAttribute("id", "btn-add");
        addToCart.classList.add("btn-add");
        addToCart.textContent = "Ajouter au panier";

        mainProduct.appendChild(img);
        mainProduct.appendChild(name);
        mainProduct.appendChild(description);
        mainProduct.appendChild(price);
        mainProduct.appendChild(options).appendChild(label);
        options.appendChild(select);
        mainProduct.appendChild(addToCart);
};

// adds product id to localStorage
const addToLocalStorage = (productId) => {
    let newCart;
    const products = localStorage.getItem("products");

    if ( products === null || products === "") {
        newCart = productId;
    } else {
        let alreadyInCart = localStorage.getItem("products");
        newCart = alreadyInCart.split();
        newCart.push(productId);
    }

    localStorage.setItem("products", newCart.toString());
};

const productId = getParamFromUrl('id');

fetchApi(`http://localhost:3000/api/teddies/${productId}`)
    .then(teddy => 
        displayProduct(teddy)
    )
    .catch(error => {
        console.log(error.message);
    });

let inCart = false;

// Listens to add to cart event and display confirmation
addToCart.addEventListener("click", function() {
    if (inCart === false) {
        addToLocalStorage(productId);
        addToCart.classList.add("btn-add--added");
        addToCart.textContent = "Ajouté !"
        inCart = true;
    }
});
