import { fetchApi, fetchApiPost, formatPrice } from "./script.js";

/* Display cart */

const display = document.getElementById("listInCart");
const total = document.getElementById("total-price");
const order = document.getElementById("order");

const displayCart = (teddy) => {
    const product = document.createElement("li");
    product.classList.add("cart");
    product.setAttribute("data-price", teddy.price);

    const img = document.createElement("img");
    img.src = teddy.imageUrl;
    img.setAttribute("alt", `Ours en peluche ${teddy.name}`);
    img.classList.add("cart__image");

    const name = document.createElement("p");
    name.classList.add("cart__name");
    name.textContent = teddy.name;

    const price = document.createElement("p");
    price.textContent = formatPrice(teddy.price);
    price.classList.add("cart__price");

    const deleteItem = document.createElement("button");
    deleteItem.textContent = "X";
    deleteItem.classList.add("cart__delete");
    deleteItem.setAttribute("id", teddy._id);

    display.appendChild(product).appendChild(img);
    product.appendChild(name);
    product.appendChild(price);
    product.appendChild(deleteItem);
}

const fetchCart = (products) => {
    const promises = products.map(productId => {
        return fetchApi(`http://localhost:3000/api/teddies/${productId}`)
            .then(teddy => {
                displayCart(teddy);
            })
            .catch(error => {
                console.log(error.message);
            });
    })
    Promise.all(promises)
        .then(() => computeTotalPrice())
}

// checks if cart is empty
const checkCart = () => {
    const products = localStorage.getItem('products');

    if (products == null || products == "") {
        const empty = document.createElement("p");
        empty.textContent = "Votre panier est vide";
        empty.setAttribute("id", "emptyCart");
        display.appendChild(empty);
        order.style.display = "none";
    } else {
        const productsInCart = products.split(',');
        fetchCart(productsInCart);
    }
}

// removes an id from localStorage
const removeFromCart = (productId) => {
    const alreadyInCart = localStorage.getItem("products");
    const oldCart = alreadyInCart.split(',');
    const newCart = oldCart.filter((id) => id != productId);
    localStorage.setItem("products", newCart.toString());
}

let totalPrice = 0;

// computes total price
const computeTotalPrice = () => {
    const products = document.querySelectorAll(".cart");
    products.forEach(product => {
        let data = product.getAttribute("data-price");
        let dataInt = parseInt(data, 10);
        
        totalPrice += dataInt;
    })
    displayTotalPrice(totalPrice);
}

// displays total price
const displayTotalPrice = (price) => {
    if (price === 0 | price === null) {
        total.style.display = "none";
        checkCart();
    } else {
        total.textContent = `Prix total : ${formatPrice(price)}`;
        total.style.display = "block";
    }
}

checkCart();

// gets the id of the item to delete and removes it
display.addEventListener('click', function(e) {
    if (e.target.classList.contains('cart__delete')) {
        e.target.parentNode.remove();
        const priceToRemove = e.target.parentNode.getAttribute("data-price");
        totalPrice -= parseInt(priceToRemove, 10);
        const productId = e.target.getAttribute("id");
        removeFromCart(productId);
        displayTotalPrice(totalPrice);
    }
})

/* Order form */

const emailInput = document.getElementById("email");
const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const address = document.getElementById("address");
const city = document.getElementById("city");
const btnBuy = document.getElementById("btn-buy");

// checks user input
const checkOrder = () => {

    if (checkEmail(emailInput.value, emailInput) && checkTextInput(firstName.value, firstName) && checkTextInput(lastName.value, lastName) && checkAddress(address.value, address) && checkTextInput(city.value, city)) {
        return true;
    } else {
        return false;
    }
}

// checks input with regex and displays alerts
const regexTest = (data, regex, inputNode) => {
    const alert = inputNode.nextElementSibling;

    if (regex.test(data) === false) {
        inputNode.classList.add('error-input');
        alert.classList.remove('alert-hidden');
        alert.classList.add('alert-displayed');
        return false;
    } else {
        alert.classList.remove('alert-displayed');
        alert.classList.add('alert-hidden');
        inputNode.classList.remove('error-input');
        return true;
    }
}

// defines the email regex
const checkEmail = (data, input) => {
    const regex = new RegExp('^([a-zA-Z0-9-_.]+)@([a-zA-Z0-9-_.]+)\.([a-zA-Z]{2,5})$');

    if (regexTest(data, regex, input) === true) {
        return true;
    }
}

// defines the names and city regex
const checkTextInput = (data, input) => {
    const regex = new RegExp('^[^0-9]{2,50}$');

    if (regexTest(data, regex, input) === true) {
        return true;
    }
}

// defines the address regex
const checkAddress = (data, input) => {
    const regex = new RegExp('^.{5,200}$');

    if (regexTest(data, regex, input) === true) {
        return true;
    }
}

// continous input checks
emailInput.addEventListener("input", (e) => {
    checkEmail(e.target.value, emailInput);
});

firstName.addEventListener("input", (e) => {
    checkTextInput(e.target.value, firstName);
});

lastName.addEventListener("input", (e) => {
    checkTextInput(e.target.value, lastName);
});

address.addEventListener("input", (e) => {
    checkAddress(e.target.value, address);
});

city.addEventListener("input", (e) => {
    checkTextInput(e.target.value, city)
});
 
// on submit checks and sends data to API, then redirects to order confirmation 
btnBuy.addEventListener("click", () => {
    if (checkOrder()) {
        const contact = {
            email: emailInput.value,
            firstName: firstName.value,
            lastName: lastName.value,
            address: address.value,
            city: city.value
        };
        const products = localStorage.getItem("products").split(',');

        fetchApiPost(
            'http://localhost:3000/api/teddies/order', 
            JSON.stringify({
                contact,
                products
            }) 
        )
        .then(data => {
            window.location.href = `/confirmation.html?order=${data.orderId}&price=${totalPrice}`;
        })
        .catch(error => console.log(error.message));
    }
});
