import {fetchApi, formatPrice} from './script.js';

const listProducts = document.getElementById("list");

const displayList = (teddies) => {
    teddies.forEach(teddy => {

        const li = document.createElement("li");
        li.classList.add('product');

        const a = document.createElement("a");
        a.href = `product.html?id=${teddy._id}`;

        const div = document.createElement("div");
        div.classList.add("product__img");

        const img = document.createElement("img");
        img.src = teddy.imageUrl;
        img.setAttribute("alt", `Ours en peluche ${teddy.name}`);

        const name = document.createElement("h2");
        name.classList.add("product__name");
        name.textContent = teddy.name;

        const p = document.createElement("p");
        p.textContent = formatPrice(teddy.price);
        p.classList.add("product__price");

        listProducts.appendChild(li).appendChild(a).appendChild(div).appendChild(img);
        a.appendChild(name);
        a.appendChild(p);
    });
}

fetchApi('http://localhost:3000/api/teddies')
    .then(teddies => 
        displayList(teddies)
    )
    .catch(error => {
        console.log(error.message);
    });
