import {getParamFromUrl, formatPrice} from './script.js';

const orderId = getParamFromUrl('order');
const price = getParamFromUrl('price');

const displayId = document.getElementById("order-id");
const displayPrice = document.getElementById("order-price");

displayId.textContent = `Numéro de commande : ${orderId}`;
displayPrice.textContent = `Montant total : ${formatPrice(price)}`;

// empty cart
localStorage.clear();
